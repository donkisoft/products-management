<%--
  Created by IntelliJ IDEA.
  User: Yoshinaga
  Date: 01/22/2018
  Time: 12:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Product Detail</title>
</head>
<body>
<div align="center">
    <h1>Product Detail</h1>
    <c:if test="${product != null}">
    <form action="update" method="post">
        </c:if>
        <c:if test="${product == null}">
        <form action="insert" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <%--<caption>
                    <h2>
                        <c:if test="${product != null}">
                            Edit Product
                        </c:if>
                        <c:if test="${product == null}">
                            Add New Product
                        </c:if>
                    </h2>
                </caption>--%>
                <c:if test="${product != null}">
                    <input type="hidden" name="id" value="<c:out value='${product.productId}' />"/>
                </c:if>
                <tr>
                    <th>Product code:</th>
                    <td>
                        <input type="text" name="title" size="45"
                               value="<c:out value='${product.productCode}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Product name:</th>
                    <td>
                        <input type="text" name="author" size="45"
                               value="<c:out value='${product.productName}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Category name:</th>
                    <td>
                        <input type="text" name="price" size="5"
                               value="<c:out value='' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Sales Start Date:</th>
                    <td>
                        <input type="text" name="price" size="5"
                               value="<c:out value='' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Sales Price:</th>
                    <td>
                        <input type="text" name="price" size="5"
                               value="<c:out value='' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Cost Price:</th>
                    <td>
                        <input type="text" name="price" size="5"
                               value="<c:out value='' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Discount:</th>
                    <td>
                        <input type="text" name="price" size="5"
                               value="<c:out value='' />"
                        />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <%--<input type="submit" value="Save"/>--%>
                        <a href="/prsys">Back</a>
                        <a href="cart/add?productId=${product.productId}">Add to Cart</a>
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>
