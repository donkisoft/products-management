<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
</head>
<body>
<div align="center">
    <h1>Product List</h1>
    <table border="1">
        <tr>
            <th>Index</th>
            <th>Name</th>
            <th>Category</th>
            <th>Sale Price</th>
            <th>Detail</th>
        </tr>
        <c:forEach var="product" items="${products}" varStatus="status">
            <tr>
                <td>${status.index + 1}</td>
                <td>${product.productName}</td>
                <td></td>
                <td></td>
                <td>
                    <a href="productDetail?productId=${product.productId}">Detail</a>
                    <a href="cart/add?productId=${product.productId}">Add to Cart</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>