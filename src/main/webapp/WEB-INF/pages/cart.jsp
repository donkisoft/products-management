<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Order Detail</title>
</head>
<body>
<div align="center">
    <h1>Order Detail</h1>
    <table border="1">
        <tr>
            <th>Product Name</th>
            <th>Quantity</th>
            <th>Sales Price</th>
            <th>Total Price</th>
        </tr>
        <c:forEach var="cart" items="${listCart}" varStatus="status">
            <tr>
                <td>${cart.name}</td>
                <td>${cart.quantity}</td>
                <td>${cart.price}</td>
                <td>${cart.price * cart.quantity}
                </td>
            </tr>
        </c:forEach>
    </table>
    <br/>
    <br/>
    <br/>
    <table border="0" cellpadding="5">
        <tr>
            <th>Customer Name:</th>
            <td>
                <input type="text" name="title" size="45" />
            </td>
        </tr>
        <tr>
            <th>Address 1:</th>
            <td>
                <input type="text" name="title" size="45" />
            </td>
        </tr>
        <tr>
            <th>Address 2:</th>
            <td>
                <input type="text" name="title" size="45" />
            </td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>
                <input type="text" name="title" size="45" />
            </td>
        </tr>
        <tr>
            <th>Ship Date:</th>
            <td>
                <input type="text" name="title" size="45" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a href="/prsys">Back</a>
                <a href="/checkout">Check Out</a>
            </td>
        </tr>
    </table>
</div>
</body>
</html>