package com.donkisoft.prsys.model;

public class Product {
    private Integer productId;
    private String productCode;
    private String productName;
    private String ctgCode;
    private String salesStartDate;
    private Double salesPrice;
    private Double costPrice;
    private String discountLimit;

    public Product() {
    }

    public Product(Integer productId, String productCode, String productName, String ctgCode,
                   String salesStartDate, Double salesPrice, Double costPrice, String discountLimit) {
        this.productId = productId;
        this.productCode = productCode;
        this.productName = productName;
        this.ctgCode = ctgCode;
        this.salesStartDate = salesStartDate;
        this.salesPrice = salesPrice;
        this.costPrice = costPrice;
        this.discountLimit = discountLimit;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCtgCode() {
        return ctgCode;
    }

    public void setCtgCode(String ctgCode) {
        this.ctgCode = ctgCode;
    }

    public String getSalesStartDate() {
        return salesStartDate;
    }

    public void setSalesStartDate(String salesStartDate) {
        this.salesStartDate = salesStartDate;
    }

    public Double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public String getDiscountLimit() {
        return discountLimit;
    }

    public void setDiscountLimit(String discountLimit) {
        this.discountLimit = discountLimit;
    }
}
