package com.donkisoft.prsys.dao;

import com.donkisoft.prsys.mapper.ProductMapper;
import com.donkisoft.prsys.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
@Transactional
public class ProductDAO extends JdbcDaoSupport {
    @Autowired
    public ProductDAO(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    private int getMaxProductId() {
        String sql = "Select max(p.product_id) from Product p";

        Integer value = this.getJdbcTemplate().queryForObject(sql, Integer.class);
        if (value == null) {
            return 0;
        }
        return value;
    }

    public Product findProduct(String productCode) {
        String sql = ProductMapper.BASE_SQL + " where d.product_code = ?";

        Object[] params = new Object[]{productCode};

        ProductMapper mapper = new ProductMapper();

        Product product = this.getJdbcTemplate().queryForObject(sql, params, mapper);
        return product;
    }

    public Product findProductById(String productId) {
        String sql = ProductMapper.BASE_SQL + " where p.product_id = ?";

        Object[] params = new Object[]{productId};

        ProductMapper mapper = new ProductMapper();

        Product product = this.getJdbcTemplate().queryForObject(sql, params, mapper);
        return product;
    }

    public List<Product> listProduct() {
        String sql = ProductMapper.BASE_SQL;

        Object[] params = new Object[]{};
        ProductMapper mapper = new ProductMapper();

        List<Product> list = this.getJdbcTemplate().query(sql, params, mapper);
        return list;
    }

    public void insertProduct(String productCode, String productName) {
        /*if (findProduct(productCode) != null) {
            return;
        }*/
        String sql = "Insert into Product (product_id, product_code, product_name) "
                + " values (?,?,?) ";
        int productId = this.getMaxProductId() + 1;
        Object[] params = new Object[]{productId, productCode, productName};
        this.getJdbcTemplate().update(sql, params);
    }
}
