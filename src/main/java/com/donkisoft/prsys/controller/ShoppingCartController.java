package com.donkisoft.prsys.controller;

import com.donkisoft.prsys.dao.ProductDAO;
import com.donkisoft.prsys.model.Cart;
import com.donkisoft.prsys.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ShoppingCartController {
    @Autowired
    private ProductDAO productDAO;

    @RequestMapping(value = "cart/index")
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("shop/list");
        List<Product> productList = productDAO.listProduct();
        mav.addObject("listResult", productList);
        return mav;
    }

    @RequestMapping("cart/add")
    public ModelAndView addCart(@RequestParam String productId, HttpSession session) {

        ModelAndView mav = new ModelAndView("cart");
        Product product = productDAO.findProductById(productId);
        Cart cart = new Cart();
        List<Cart> list = (List<Cart>) session.getAttribute("cart");
        if (list == null) {
            list = new ArrayList<Cart>();
        }
        if (product != null) {
            cart.ToCart(product);
            Double total = addToCart(list, cart);
            mav.addObject("total", total);
            session.setAttribute("cart", list);
        }
        mav.addObject("listCart", list);
        return mav;
    }

    private Double addToCart(List<Cart> list, Cart cart) {
        Double total = 0.0;
        boolean isExit = false;
        for (Cart c : list) {
            if (c.equals(cart)) {
                c.setQuantity(c.getQuantity() + 1);
                isExit = true;
            }
            total = total + c.getPrice() * c.getQuantity();
        }
        if (isExit == false) {
            list.add(cart);
            total = total + cart.getPrice() * cart.getQuantity();
        }
        return total;
    }

    @RequestMapping("cart/remove")
    public ModelAndView removeCart(@RequestParam int id, HttpSession session) {
        ModelAndView mav = new ModelAndView("cart");
        List<Cart> list = (List<Cart>) session.getAttribute("cart");
        if (list != null) {
            Double total = removeCartItem(list, id);
            mav.addObject("total", total);
            session.setAttribute("cart", list);
        }
        mav.addObject("listCart", list);
        return mav;
    }

    private Double removeCartItem(List<Cart> list, int id) {
        Double total = 0.0;
        Cart temp = null;
        for (Cart c : list) {
            if (c.getId() == (id)) {
                temp = c;
                continue;
            }
            total = total + c.getPrice() * c.getQuantity();
        }
        if (temp != null)
            list.remove(temp);
        return total;
    }

    @RequestMapping("cart/update")
    public ModelAndView updateCart(@RequestParam int id,
                                   @RequestParam int quantity,
                                   HttpSession session) {
        ModelAndView mav = new ModelAndView("cart");
        List<Cart> list = (List<Cart>) session.getAttribute("cart");
        if (list != null) {
            Double total = updateCartItem(list, id, quantity);
            mav.addObject("total", total);
            session.setAttribute("cart", list);
        }
        mav.addObject("listCart", list);
        return mav;
    }

    private Double updateCartItem(List<Cart> list, int id, int quantity) {
        Double total = 0.0;
        for (Cart c : list) {
            if (c.getId() == (id)) {
                c.setQuantity(quantity);
            }
            total = total + c.getPrice() * c.getQuantity();
        }
        return total;
    }
}
