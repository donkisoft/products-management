package com.donkisoft.prsys.controller;

import com.donkisoft.prsys.dao.ProductDAO;
import com.donkisoft.prsys.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ProductDAO productDAO;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String productList(Model model) {
        /*productDAO.insertProduct("IP6", "iPhone 6");
        productDAO.insertProduct("IP6P", "iPhone 6 Plus");*/
        List<Product> list = productDAO.listProduct();
        model.addAttribute("products", list);
        return "productList";
    }
    @RequestMapping(value = {"/productDetail"}, method = RequestMethod.GET)
    public String productEdit(Model model, String productId) {
        model.addAttribute("product", productDAO.findProductById(productId));
        return "productDetail";
    }
}
