package com.donkisoft.prsys.mapper;

import com.donkisoft.prsys.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements RowMapper<Product> {
    public static final String BASE_SQL = "Select p.product_id, p.product_code, p.product_name, " +
            "p.ctg_code, p.sales_start_date, p.sales_price, p.cost_price, p.discount_limit " +
            "from Product p ";

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Integer productId = rs.getInt("product_id");
        String productCode = rs.getString("product_code");
        String productName = rs.getString("product_name");
        String ctgCode = rs.getString("ctg_code");
        String salesStartDate = rs.getString("sales_start_date");
        Double salesPrice = rs.getDouble("sales_price");
        Double costPrice = rs.getDouble("cost_price");
        String discountLimit = rs.getString("discount_limit");

        return new Product(productId, productCode, productName, ctgCode,
                salesStartDate, salesPrice, costPrice, discountLimit);
    }
}
